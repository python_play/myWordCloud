# 趣玩python系列五 使用word_cloud 生成词云

#### 介绍
使用word_cloud 生成词云

本组织所有项目都趣玩Python系列子项目，所有代码为原创，或标记转载，如有侵权，请邮件mtong@foxmail.com进行告知
软件版本 python3.7.4

#### 软件架构
code_rain.py 主要执行文件
article.txt
base_show.jpeg 背景图
字体文件根据实际自己的环境进行绝对路径的填写
MAC系统在：/System/Library/Fonts/Supplemental/；
Windows系统在 C:/widnwos/Fonts

#### 安装教程

1.  pip3 install pygame
2.  pip3 install collections
3.  pip3 install jieba
4.  pip3 install wordcloud
5.  pip3 install matplotlib
6.  pip3 install numpy

#### 使用说明

python3 main.py

#### 关于作者

今日头条: [明哥读世界(Jairry)](https://www.toutiao.com/c/user/81608860015/)

微博：[明哥读世界](https://weibo.com/6008105952)

个人主页：[www.mylasting.com](https://www.mylasting.com)