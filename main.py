#!/usr/bin/env python
# _*_coding:utf-8 _*_
#@Time    :2019/5/21 6:06 PM
#@Author  :Jairry 
#@FileName: main.py

#@Software: PyCharm
import re
import collections
import jieba
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
my_words=[
        '李子柒',
        # '姚记扑克',
    ]
remove_words = [
    u'的', u'，',
    u'年',
    u'2018',

]
def showGenerateFrequencies(string_data,img):
    seg_list_exact = jieba.cut(string_data, cut_all=False)  # 精确模式分词
    object_list=[]
    for key in my_words:
        jieba.add_word(key)
    for word in seg_list_exact:  # 循环读出每个分词
        if word not in remove_words:  # 如果不在去除词库中
            object_list.append(word)  # 分词追加到列表
    word_lists = collections.Counter(object_list)  # 对分词做词频统计
    # generate: 词云
    # 生成词云资源，重要参数：font_path为字体，本文使用为mac系统文字，windows可使用绝对路径的文字进行显示， background 图片的背景色，mask 图片资源
    wc = WordCloud(font_path='STHeiti Medium.ttc', width=800, height=600, mode='RGBA', background_color="white",
                   mask=img).generate_from_frequencies(word_lists)
    plt.imshow(wc, interpolation='bilinear')
    # 去除坐标轴
    plt.axis('off')
    plt.show()

    wc.to_file('showGenerateFrequencies.png')

def show_generate(string_data,img):
    # generate: 由wordCloud直接分词
    wc = WordCloud(font_path='STHeiti Medium.ttc',width=800, height=600, mode='RGBA', background_color="white",mask=img).generate(string_data)
    plt.imshow(wc, interpolation='bilinear')
    plt.axis('off')
    plt.show()
    # 保存到文件
    wc.to_file('show_generate.png')

def get_a(d):
    return d[0]
if __name__ == '__main__':
    #打开背景图资源
    img = Image.open('base_show.jpeg')
    img = np.array(img)

    #读取图片资源
    fn = open('article.txt', encoding='utf-8')  # 打开文件
    string_data = fn.read()  # 读出整个文件
    fn.close()  # 关闭文件
    # 文本预处理
    pattern = re.compile(u'\t|\n|\.|-|:|;|\)|\(|\?|"')  # 定义正则表达式匹配模式
    string_data = re.sub(pattern, '', string_data)  # 将符合模式的字符去除
    #demo 1

    # show_generate(string_data,img)

    # demo 2
    showGenerateFrequencies(string_data,img)


